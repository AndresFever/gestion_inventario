package Vista;

import Controlador.Controlador_Inventario_producto;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author JEstebanC <esteban-01997@hotmail.com>
 */
public class Inventario_producto {

    DefaultTableModel modelo;
    public JTable tabla;
    JScrollPane scroll;
    public JButton jbAnadir, jbBorrar;

    Controlador_Inventario_producto controlador;

    public Inventario_producto(GestionInventario gestionInventario) {

        JLabel jlTitulo = new JLabel("INVENTARIO PRODUCTOS");
        jlTitulo.setForeground(new Color(112, 0, 0));
        jlTitulo.setBounds(20, 20, 550, 30);
        jlTitulo.setFont(new Font("Arial", Font.BOLD, 35));

        modelo = new DefaultTableModel(new Object[][]{},
                new String[]{
                    "Código producto ", "Nombre Producto", "Descripcion", "Cantidad", "Seleccionar"
                }) {
            Class[] columnTypes = new Class[]{
                Object.class, Object.class, Object.class, Object.class, Boolean.class
            };
            boolean[] editable = {false, false, false, false, true};
        };

        tabla = new JTable(modelo);
        scroll = new JScrollPane(tabla, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setBounds(30, 200, 1000, 370);
        tabla.getTableHeader().setReorderingAllowed(false);
        tabla.setRowHeight(60);

        jbAnadir = new JButton("Añadir Producto");
        jbAnadir.setBounds(270, 650, 200, 30);
        jbAnadir.setFont(new Font("Tahoma", Font.PLAIN, 15));
        jbAnadir.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbAnadir.setHorizontalTextPosition(SwingConstants.LEFT);
        jbAnadir.setVerticalTextPosition(SwingConstants.CENTER);

        jbBorrar = new JButton("Eliminar Producto");
        jbBorrar.setBounds(550, 650, 200, 30);
        jbBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
        jbBorrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbBorrar.setHorizontalTextPosition(SwingConstants.LEFT);
        jbBorrar.setVerticalTextPosition(SwingConstants.CENTER);

        controlador = new Controlador_Inventario_producto(this);
        jbAnadir.addActionListener(controlador);
        jbBorrar.addActionListener(controlador);

        gestionInventario.jpInventario_producto.add(jlTitulo);
        gestionInventario.jpInventario_producto.add(scroll);
        gestionInventario.jpInventario_producto.add(jbAnadir);
        gestionInventario.jpInventario_producto.add(jbBorrar);

        gestionInventario.jpInventario_producto.repaint();
        gestionInventario.jpInventario_producto.setVisible(true);
        gestionInventario.jpDerecho.setVisible(false);
    }

}
