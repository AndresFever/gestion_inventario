package Vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 *
 * @author JEstebanC <esteban-01997@hotmail.com>
 */
public class Vista_Nuevo_producto extends JDialog {

    public Vista_Nuevo_producto(JDialog a, boolean al) {
        super(a, al);
        setTitle("Nuevo Producto");
        GUI();
        setSize(280, 200);
        setLayout(null);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
    JPanel jpPanel;
    JTextField jtfNuevo_producto, jtfDescripcion;
    JComboBox jcbCategoria;
    JButton jbAgregar;

    private void GUI() {
        jpPanel = new JPanel();
        jpPanel.setLayout(null);
        jpPanel.setBounds(0, 0, 280, 200);
        jpPanel.setBackground(Color.white);

        JLabel jlNuevo_producto = new JLabel("Nuevo Producto");
        jlNuevo_producto.setForeground(new Color(112, 0, 0));
        jlNuevo_producto.setBounds(10, 10, 150, 20);
        jlNuevo_producto.setFont(new Font("Arial", Font.PLAIN, 15));

        JLabel jlDescripcion = new JLabel("Descripcion");
        jlDescripcion.setForeground(new Color(112, 0, 0));
        jlDescripcion.setBounds(10, 50, 90, 20);
        jlDescripcion.setFont(new Font("Arial", Font.PLAIN, 15));

        JLabel jlCategoria = new JLabel("Categoria");
        jlCategoria.setForeground(new Color(112, 0, 0));
        jlCategoria.setBounds(10, 90, 90, 20);
        jlCategoria.setFont(new Font("Arial", Font.PLAIN, 15));

        jtfNuevo_producto = new JTextField();
        jtfNuevo_producto.setBounds(130, 10, 120, 20);
        jtfNuevo_producto.setFont(new Font("Arial", Font.PLAIN, 15));
        jtfNuevo_producto.setForeground(new Color(112, 0, 0));

        jtfDescripcion = new JTextField();
        jtfDescripcion.setBounds(130, 50, 120, 20);
        jtfDescripcion.setFont(new Font("Arial", Font.PLAIN, 15));
        jtfDescripcion.setForeground(new Color(112, 0, 0));

        jcbCategoria = new JComboBox();
        jcbCategoria.setBounds(130, 90, 120, 20);
        jcbCategoria.setFont(new Font("Arial", Font.PLAIN, 15));
        jcbCategoria.setForeground(new Color(112, 0, 0));

        jbAgregar = new JButton("Agregar Producto");
        jbAgregar.setBounds(60, 120, 150, 30);
        jbAgregar.setFont(new Font("Tahoma", Font.PLAIN, 15));
        jbAgregar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbAgregar.setHorizontalTextPosition(SwingConstants.LEFT);
        jbAgregar.setVerticalTextPosition(SwingConstants.CENTER);

        add(jpPanel);
        jpPanel.add(jlNuevo_producto);
        jpPanel.add(jlDescripcion);
        jpPanel.add(jlCategoria);
        jpPanel.add(jtfNuevo_producto);
        jpPanel.add(jtfDescripcion);
        jpPanel.add(jcbCategoria);
        jpPanel.add(jbAgregar);

    }

}
