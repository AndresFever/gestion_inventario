package Vista;

import Controlador.Controlador_GestionInventario;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author Velez
 */
public class GestionInventario extends JFrame {

    public JFrame jframe = new JFrame();
    public JPanel jpIzquierdo, jpDerecho,jpInventario_producto;
    public JButton jb1, jb2, jb3, jb4, jb5;
    public JLabel jlEtiqueta;

//    Tamano pantalla 
    private final java.awt.Toolkit toolkitPantalla = java.awt.Toolkit.getDefaultToolkit();
    private final java.awt.Dimension dimeTamano = toolkitPantalla.getScreenSize();
    private final int nAltura = dimeTamano.height;
    private final int nAncho = dimeTamano.width;

    Controlador_GestionInventario controlador;

    public GestionInventario() {
        super("Gestion Inventario");
        GUI();
        jframe.setSize(nAncho, nAltura);
        jframe.setLayout(null);
        jframe.setLocationRelativeTo(null);
        jframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
        jframe.setResizable(false);
        jframe.setVisible(true);
    }

    private void GUI() {
        jpIzquierdo = new JPanel();
        jpIzquierdo.setLayout(null);
        jpIzquierdo.setBounds(0, 0, 300, 700);
        jpIzquierdo.setBackground(Color.white);

        jpDerecho = new JPanel();
        jpDerecho.setLayout(null);
        jpDerecho.setBounds(300, 0, nAncho-300, nAltura);
        jpDerecho.setBackground(Color.gray);
        
        jpInventario_producto = new JPanel();
        jpInventario_producto.setLayout(null);
        jpInventario_producto.setBounds(300, 0, nAncho-300, nAltura);
        jpInventario_producto.setBackground(Color.gray);
        jpInventario_producto.setVisible(false);

        jb1 = new JButton("Boton 1");
        jb1.setBounds(25, 150, 250, 30);
        jb1.setBorder(null);
        jb1.setFont(new Font("Arial", Font.PLAIN, 15));
        jb1.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jb1.setHorizontalTextPosition(SwingConstants.LEFT);
        jb1.setVerticalTextPosition(SwingConstants.CENTER);
        jpIzquierdo.add(jb1);

        jb2 = new JButton("Boton 2");
        jb2.setBounds(25, 220, 250, 30);
        jb2.setBorder(null);
        jb2.setFont(new Font("Arial", Font.PLAIN, 15));
        jb2.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jb2.setHorizontalTextPosition(SwingConstants.LEFT);
        jb2.setVerticalTextPosition(SwingConstants.CENTER);
        jpIzquierdo.add(jb2);

        jb3 = new JButton("Boton 3");
        jb3.setBounds(25, 290, 250, 30);
        jb3.setBorder(null);
        jb3.setFont(new Font("Tahoma", Font.PLAIN, 15));
        jb3.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jb3.setHorizontalTextPosition(SwingConstants.LEFT);
        jb3.setVerticalTextPosition(SwingConstants.CENTER);
        jpIzquierdo.add(jb3);

        jb4 = new JButton("Boton 4");
        jb4.setBounds(25, 360, 250, 30);
        jb4.setBorder(null);
        jb4.setFont(new Font("Tahoma", Font.PLAIN, 15));
        jb4.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jb4.setHorizontalTextPosition(SwingConstants.LEFT);
        jb4.setVerticalTextPosition(SwingConstants.CENTER);
        jpIzquierdo.add(jb4);

        jb5 = new JButton("Boton 5");
        jb5.setBounds(25, 430, 250, 30);
        jb5.setBorder(null);
        jb5.setFont(new Font("Tahoma", Font.PLAIN, 15));
        jb5.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jb5.setHorizontalTextPosition(SwingConstants.LEFT);
        jb5.setVerticalTextPosition(SwingConstants.CENTER);
        jpIzquierdo.add(jb5);

        jlEtiqueta = new JLabel("Etiqueta 1");
        jlEtiqueta.setBounds(25, 40, 300, 40);
        jlEtiqueta.setFont(new Font("Arial", Font.PLAIN, 15));
        jpIzquierdo.add(jlEtiqueta);

        controlador = new Controlador_GestionInventario(this);
        jb1.addActionListener(controlador);
        jb2.addActionListener(controlador);
        jb3.addActionListener(controlador);
        jb4.addActionListener(controlador);
        jb5.addActionListener(controlador);

        jframe.add(jpIzquierdo);
        jframe.add(jpDerecho);
        jframe.add(jpInventario_producto);
    }

    public static void main(String[] args) {
        GestionInventario gestionInventario = new GestionInventario();
    }

}
