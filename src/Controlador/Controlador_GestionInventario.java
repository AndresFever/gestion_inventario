package Controlador;

import Vista.GestionInventario;
import Vista.Inventario_producto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author JEstebanC <esteban-01997@hotmail.com>
 */
public class Controlador_GestionInventario implements ActionListener {

    GestionInventario gestionInventario;

    public Controlador_GestionInventario(GestionInventario gestionInventario) {
        this.gestionInventario = gestionInventario;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource().equals(gestionInventario.jb1)) {
            Inventario_producto ip = new Inventario_producto(gestionInventario);
        }
    }

}
